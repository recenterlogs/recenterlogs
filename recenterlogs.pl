#!/usr/bin/perl
#
# Copyright (c) 2010  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

use strict;
use warnings;

use File::Basename;
use Getopt::Std;
use Text::Iconv;

my ($debug, $verbose, $encfrom, $encto) = (0, 0, 'windows-1251', 'UTF-8');
my ($i_u2u, $i_f2t, $eof);

sub convert_file($ $);
sub convert_message($ $);
sub init_encodings();
sub read_message($);
sub mkdir_p($);

sub debug($);
sub rlog($);
sub usage($);
sub version();

# Function:
#	debug			- display a diagnostic message
# Inputs:
#	$msg			- the message to display if needed
# Returns:
#	True.
# Modifies:
#	Writes to stderr if needed.
#
sub debug($)
{
	print STDERR "RDBG $_[0]\n" if $debug;
}

# Function:
#	rlog			- display a progress message
# Inputs:
#	$msg			- the message to display if needed
# Returns:
#	True.
# Modifies:
#	Writes to stderr if needed.
#
sub rlog($)
{
	print STDERR "$_[0]\n" if $verbose;
}

# Function:
#	usage			- display program usage information
# Inputs:
#	$ferr			- is this due to error or -h?
# Returns:
#	Dies if $ferr, true otherwise.
# Modifies:
#	Writes to stdout or stderr, dies if $ferr.
#
sub usage($)
{
	my ($ferr) = @_;
	my $s = <<EOUSAGE
Usage:	recenterlogs [-dv] [-D outdir | -o outfile] [-f encoding] logfile...
	recenterlogs [-hV]

	-D	output to separate files in the specified directory
	-d	debug operation - display diagnostic output
	-f	convert from the specified encoding (default: windows-1251)
	-h	display program usage information and exit
	-o	output to the specified file instead of standard output
	-V	display program version information and exit
	-v	verbose operation - display progress output
EOUSAGE
	;

	if ($ferr) {
		die($s);
	} else {
		print $s;
	}
}

sub version()
{
	print "recenterlogs 0.01\n";
}

# Function:
#	read_message		- read a single message from the logfile
# Inputs:
#	$infile			- the logfile to read from
# Returns:
#	The message on success, undef on EOF or error.
# Modifies:
#	Sets $eof on EOF; reads from the input file.
#
sub read_message($)
{
	my ($infile) = @_;
	my ($line, $msg);

	debug("read_message() invoked");
	return undef if $eof;
	do {
		$line = <$infile>;
		if (!defined($line) || !length($line)) {
			$eof = 1;
			return $msg;
		}
		$msg .= $line;
	} until ($line =~ /^\f[\r\n]*$/);
	return $msg;
}

sub is_utf8($)
{
	my ($msg) = @_;
	my $res;

	eval {
		$res = $i_u2u->convert($msg);
	};
	if ($@) {
		return 0;
	} else {
		return defined($res);
	}
}

# Function:
#	convert_message		- convert and output a single message
# Inputs:
#	$outfile		- the output logfile to write to
#	$msg			- the message to convert
# Returns:
#	True on success, always dies on error.
# Modifies:
#	Converts the message and writes to $outfile.
#
sub convert_message($ $)
{
	my ($outfile, $msg) = @_;
	my ($is_utf8, $res);

	debug("convert_message(".length($msg).") invoked");
	$is_utf8 = is_utf8($msg);
	debug("- is_utf8 $is_utf8");

	if ($encto eq 'UTF-8' && $is_utf8) {
		print $outfile $msg;
		return 1;
	}
	eval {
		$res = $i_f2t->convert($msg);
	};
	if ($@) {
		die("Error converting a message: $@\n");
	} elsif (!defined($res)) {
		die("Could not convert a message from $encfrom to $encto\n");
	}
	print $outfile $res;
}

sub init_encodings()
{
	my ($u, $f);

	debug("init_encodings('$encfrom', '$encto') invoked");
	foreach ($encfrom, $encto) {
		$_ = 'UTF-8' if /^utf-?8$/i;
	}
	debug(" - now '$encfrom', '$encto'");
	$u = Text::Iconv->new('UTF-8', 'UNICODE') or
	    die("Could not initialize the UTF-8 to UNICODE conversion\n");
	debug("- u2u: $u");
	$f = Text::Iconv->new($encfrom, $encto) or
	    die("Could not initialize the $encfrom to $encto conversion\n");
	debug("- f2t: $f");
	$i_u2u = $u;
	$i_f2t = $f;
}

# Function:
#	convert_file		- convert a single logfile
# Inputs
#	$fname			- the input filename or undef for stdin
#	$outfile		- the output file handle
# Returns:
#	True on success, always dies on error.
# Modifies:
#	Opens and closes the input file if needed.
#	Invokes read_message() and convert_message() in a loop.
#
sub convert_file($ $)
{
	my ($fname, $outfile) = @_;
	my ($infile, $f, $msg);

	debug("convert_file(".(defined($fname)? $fname: '(none)').
	    ", $outfile) invoked");
	if (defined($fname)) {
		open($f, '<', $fname) or
		    die("Opening input file '$fname': $!\n");
		$infile = $f;
	} else {
		debug("- using stdin");
		$infile = \*STDIN;
	}

	undef $eof;
	eval {
		convert_message($outfile, $msg)
		  while $msg = read_message($infile);
	};
	$msg = $@;
	debug("convert_file - finished the conversion, msg is '$msg'");

	if ($infile != \*STDIN) {
		close($infile) or
		    warn("Closing input file '$fname': $!\n");
	}
	die $msg if $msg;
}

# Function:
#	mkdir_p			- create a directory and its parents as needed
# Inputs:
#	$path			- the path to the final directory to create
# Returns:
#	True on success, undef with $! set on mkdir() error.
# Modifies:
#	Nothing; creates directories.
#
sub mkdir_p($)
{
	my ($path) = @_;
	my ($dir, $name);

	return 1 if -d $path;
	return undef if -e $path;
	return 1 if $path =~ m{^/*$};
	return undef unless mkdir_p(dirname($path));
	return mkdir($path);
}

MAIN:
{
	my ($msg, $outdir, $outfname, $outfile);
	my %opts;

	getopts('D:df:ho:Vv', \%opts) or usage(1);
	version() if $opts{'V'};
	usage(0) if $opts{'h'};
	exit(0) if $opts{'h'} || $opts{'V'};

	$debug = 1 if $opts{'d'};
	$verbose = 1 if $opts{'v'};
	if (defined($opts{'o'}) && defined($opts{'D'})) {
		die("The -D and -o options may not be used simultaneously\n");
	}
	$outfname = $opts{'o'};
	$outdir = $opts{'D'};
	$encfrom = $opts{'f'} if defined($opts{'f'});

	init_encodings();
	if (defined($outfname)) {
		open($outfile, '>', $outfname) or
		    die("Opening output file '$outfname': $!\n");
	} elsif (defined($outdir)) {
		# Strip the trailing slashes, but allow '/'
		if ($outdir =~ m{^(.+?)/+$}) {
			$outdir = $1;
		}
		if (! -d $outdir) {
			die("Nonexistent output directory '$outdir'\n");
		}
	} else {
		$outfname = 'standard output';
		$outfile = \*STDOUT;
	}
	if (!@ARGV) {
		if (defined($outdir)) {
			die("Cannot convert standard input to a directory\n");
		}
		rlog("Converting standard input to $outfname");
		convert_file(undef, $outfile);
	} else {
		foreach (@ARGV) {
			if (defined($outdir)) {
				if (m{(^|/)\.\./}) {
					die("'..' not allowed in input file ".
					    "names when -D is in effect\n");
				}
				$outfname = "$outdir/$_";
				mkdir_p(dirname($outfname)) or
				    die("Creating the parent directories ".
				    "for '$outfname': $!\n");
				open($outfile, '>', $outfname) or
				    die("Opening output file '$outfname': ".
				    "$!\n");
			}
			rlog("Converting $_ to $outfname");
			convert_file($_, $outfile);
			if (defined($outdir)) {
				close($outfile) or
				    die("Closing output file '$outfname': ".
				    "$!\n");
				undef $outfile;
			}
		}
	}
	if (defined($outfile) && $outfile != \*STDOUT) {
		close($outfile) or
		    die("Closing output file '$outfname': $!\n");
	}
}
